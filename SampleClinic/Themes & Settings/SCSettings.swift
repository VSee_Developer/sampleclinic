//
//  BaseAppSettings.swift
//  SampleClinic
//
//  Created by dat on 11/21/17.
//  Copyright © 2017 dat. All rights reserved.
//

import Foundation
import VSeeClinicKit

class SCSettings: ClinicSettings {
    
    static override var settings: SCSettings {
        return ClinicSettings.settings as! SCSettings
    }

    override var apiKey: String { return "your-api-key" }       // To be added before going to production

    override var networkBaseUrl: String {
        return "https://api-sidra.vseepreview.com/cc/next/api_v3"
    }
    
}

