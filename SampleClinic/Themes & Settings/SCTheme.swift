//
//  OrthoLiveTheme.swift
//  VC
//
//  Created by Ken on 6/6/17.
//  Copyright © 2017 vsee. All rights reserved.
//

import Foundation
import VSeeClinicKit

class SCTheme : ClinicTheme {
    var windowTintColor: UIColor {
        return UIColor(red: 0.0/255.0, green: 173.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }

    var navigationBackgroundStartColor: UIColor? {
        return UIColor(red: 0.0/255.0, green: 173.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }
    var navigationBackgroundEndColor: UIColor? {
        return UIColor(red: 0.0/255.0, green: 173.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }
    var submitButtonStartColor: UIColor {
        return UIColor(red: 0.0/255.0, green: 173.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }
    var submitButtonEndColor: UIColor {
        return UIColor(red: 0.0/255.0, green: 173.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }

    var signUpBackgroundColor: UIColor? {
        return UIColor.groupTableViewBackground
    }
    var signUpTextColor: UIColor? {
        return UIColor(red: 0.0/255.0, green: 173.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }
    
    func customizeTheme() {
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}

