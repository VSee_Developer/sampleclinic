//
//  LoginViewController.swift
//  VC
//
//  Created by Ken Tran on 4/7/16.
//  Copyright © 2016 vsee. All rights reserved.
//

import UIKit
import MBProgressHUD
import VSeeClinicKit


class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var proceedAsGuestButton: UIButton?
    
    lazy var clinic: Clinic = {
        let clinic = Clinic(roomCode: "sidra")         // Your room code
        return clinic
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.text = "sidra.patient.01"
        passwordTextField.text = "@Password123"
    }

    @IBAction func login(_ sender: AnyObject) {
        guard let userName = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        ClinicAuth.login(userName: userName, password: password, success: { (auth) in
            hud.hide(animated: true)
            self.clinic.authentication = auth
            if let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: HomeViewController.self)) as? HomeViewController {
                homeVC.clinic = self.clinic
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
        }) { (error) in
            hud.hide(animated: true)
            self.showErrorAlert(nil, message: error.localizedDescription)
        }
    }
    
    @IBAction func proceedAsGuest(_ sender: Any) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        clinic.guestWalkIn(fromViewController: self, success: {
            hud.hide(animated: true)
        }) { (error) in
            hud.hide(animated: true)
            self.showErrorAlert(nil, message: error.localizedDescription)
        }
    }
    
}
