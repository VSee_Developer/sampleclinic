//
//  HomeVC.swift
//  SampleClinic
//
//  Created by dat on 11/22/17.
//  Copyright © 2017 dat. All rights reserved.
//

import UIKit
import MBProgressHUD
import VSeeClinicKit

class HomeViewController: UIViewController {

    var clinic: Clinic?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func seeDoctor(_ sender: AnyObject) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        clinic?.memberWalkIn(fromViewController: self, success: {
            hud.hide(animated: true)
        }, failure: { (error) in
            hud.hide(animated: true)
            self.showErrorAlert(nil, message: error.localizedDescription)
        })
    }
    
    @IBAction func schedule(_ sender: AnyObject) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        clinic?.scheduleAppointment(fromViewController: self, success: {
            hud.hide(animated: true)
        }, failure: { (error) in
            hud.hide(animated: true)
            self.showErrorAlert(nil, message: error.localizedDescription)
        })
    }
    
    @IBAction func logoutBtnClicked(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        clinic?.authentication?.logout(success: {
            hud.hide(animated: true)
            self.navigationController?.popToRootViewController(animated: true)
        }, failure: { (error) in
            hud.hide(animated: true)
            self.showErrorAlert(nil, message: error.localizedDescription)
        })
    }
}
